# Developer Tools

These tools / scripts are for the developer to check code quality and build locally.

## Notes

* At this time, it is assumed that development is occurring within a Linux environment.
* I (the author, Joey Rockhold), develop using Conda (Miniconda specifically), but this is not a requirement.
* At this time, Easy ANSI development must work with Python versions 3.6 and higher.

## Interactive Tools

* Run 'menu.sh' from the root directory.
* To configure the interactive tools, see './dev_tools/tools/config.py'
* The bash scripts are coded so that they can be run independently.
  * This allows CI tools to use the same scripts with no changes.
* For a fast exit at any time, press Ctrl + C
