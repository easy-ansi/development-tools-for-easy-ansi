#!/usr/bin/env bash
set -e

CURRENT_DIR=$(pwd)
PYTHON_INTERPRETER=""


function main() {
    find_python_interpreter
    cd ./dev_tools
    eval "${PYTHON_INTERPRETER} dev_tools.py"
    cd "${CURRENT_DIR}"
}


# Find the Python Interpreter to use
function find_python_interpreter() {
    check_python_interpreter "python"
    if [ "${PYTHON_INTERPRETER}" == "" ]
    then
        check_python_interpreter "python3"
    fi
    if [ "${PYTHON_INTERPRETER}" == "" ]
    then
        echo "ERROR: Python 3 Interpreter not found"
        exit 1
    fi
}


function check_python_interpreter() {
    local py_inter
    py_inter="${1}"
    local status
    set +e
    which "${py_inter}">/dev/null 2>&1
    status=$?
    set -e
    if [ "${status}" -eq 0 ]
    then
        local major_version
        major_version=$(eval "${py_inter} --version|cut -f2 -d\ |cut -c1")
        if [ "${major_version}" == "3" ]
        then
            PYTHON_INTERPRETER="${py_inter}"
        fi
    fi
}


main
