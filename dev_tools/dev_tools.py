from menus.main_menu import main_menu
from tools.text import color16


def run():
    try:
        main_menu()
    except KeyboardInterrupt:
        print("\nKeyboard Interrupt\n")
    finally:
        default_colors = color16(-1, -1)
        print(f"{default_colors}\nDone\n")


if __name__ == "__main__":
    run()
