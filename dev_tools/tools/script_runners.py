import subprocess
import sys
from tools.text import heading, press_enter_to_continue, yes_no
from tools import config as cfg


def run_pytest(project: str):
    # heading
    hdg = f"Run PyTest: {project}"
    print(heading(hdg))
    # run pytest script
    start_dir = cfg.CODE_QUALITY_LINUX_SCRIPTS_DIR
    project_dir = cfg.PROJECTS[project]["base_dir"]
    source_dir = f"{project_dir}/{cfg.PROJECTS[project]['source_dir']}"
    tests_dir = f"{project_dir}/{cfg.PROJECTS[project]['tests_dir']}"
    command = f'./pytest.sh "{cfg.PYTHON_ENV}" "{project_dir}" "{source_dir}" "{tests_dir}"'
    run_script(command, start_dir)


def run_mypy(project: str):
    # heading
    hdg = f"Run MyPy: {project}"
    print(heading(hdg))
    # run mypy script
    start_dir = cfg.CODE_QUALITY_LINUX_SCRIPTS_DIR
    project_dir = cfg.PROJECTS[project]["base_dir"]
    source_dir = f"{project_dir}/{cfg.PROJECTS[project]['source_dir']}"
    mypy_ini_file = f"{start_dir}/mypy/mypy.ini"
    command = f'./mypy.sh "{cfg.PYTHON_ENV}" "{source_dir}" "{mypy_ini_file}"'
    run_script(command, start_dir)


def run_flake8(project: str):
    # heading
    hdg = f"Run Flake8: {project}"
    print(heading(hdg))
    # run flake8 script
    start_dir = cfg.CODE_QUALITY_LINUX_SCRIPTS_DIR
    project_dir = cfg.PROJECTS[project]["base_dir"]
    source_dir = f"{project_dir}/{cfg.PROJECTS[project]['source_dir']}"
    flake8_ini_file = f"{start_dir}/flake8/flake8.ini"
    command = f'./flake8.sh "{cfg.PYTHON_ENV}" "{source_dir}" "{flake8_ini_file}"'
    run_script(command, start_dir)


def install_locally(project: str):
    # heading
    hdg = f"Install Locally: {project}"
    print(heading(hdg))
    # run installer
    start_dir = cfg.BUILD_AND_DEPLOY_LINUX_SCRIPTS_DIR
    project_dir = cfg.PROJECTS[project]["base_dir"]
    command = f'./install_locally.sh "{cfg.PYTHON_ENV}" "{project_dir}"'
    run_script(command, start_dir)


def full_build(project: str):
    # heading
    hdg = f"Full Build Locally: {project}"
    print(heading(hdg))
    # run installer
    start_dir = cfg.BUILD_AND_DEPLOY_LINUX_SCRIPTS_DIR
    project_dir = cfg.PROJECTS[project]["base_dir"]
    command = f'./full_build.sh "{cfg.PYTHON_ENV}" "{project_dir}"'
    run_script(command, start_dir)


def clean_build_files(project: str):
    # heading
    hdg = f"Clean (Delete) Local Build Files: {project}"
    print(heading(hdg))
    # run installer
    start_dir = cfg.BUILD_AND_DEPLOY_LINUX_SCRIPTS_DIR
    project_dir = cfg.PROJECTS[project]["base_dir"]
    egg_name = cfg.PROJECTS[project]["egg_name"]
    command = f'./clean_build.sh "{cfg.PYTHON_ENV}" "{project_dir}" "{egg_name}"'
    run_script(command, start_dir)


def publish_pypi(project: str, test: bool):
    # heading
    hdg = None
    if test:
        hdg = f"Deploy to Test PyPI: {project}"
    else:
        hdg = f"Deploy to PRODUCTION PyPI: {project}"
    print(heading(hdg))
    # rebuild if needed
    rebuild = yes_no(f"Do you want to rebuild {project}?")
    if rebuild:
        clean_build_files(project)
        full_build(project)
        print(heading(hdg))
    # run publisher
    start_dir = cfg.BUILD_AND_DEPLOY_LINUX_SCRIPTS_DIR
    project_dir = cfg.PROJECTS[project]["base_dir"]
    pypi_token_file = None
    pypi_environment = None
    if test:
        pypi_token_file = f'{cfg.PYPI_CREDENTIALS_DIR}/{cfg.PROJECTS[project]["test_pypi_token_file"]}'
        pypi_environment = "test"
    else:
        pypi_token_file = f'{cfg.PYPI_CREDENTIALS_DIR}/{cfg.PROJECTS[project]["prod_pypi_token_file"]}'
        pypi_environment = "PRODUCTION"
    command = f'./publish_pypi.sh "{cfg.PYTHON_ENV}" "{project_dir}" "{pypi_environment}" "{pypi_token_file}"'
    run_script(command, start_dir)


def run_script(command: str, start_dir: str, post_pause: bool = True):
    # https://docs.python.org/3.6/library/subprocess.html#replacing-os-system
    try:
        return_code = subprocess.call(command, shell=True, cwd=start_dir)
        if return_code < 0:
            print("Child was terminated by signal", -return_code, file=sys.stderr)
        elif return_code > 0:
            print("Child non-zero return code", return_code, file=sys.stderr)
    except OSError as e:
        print("Execution failed:", e, file=sys.stderr)
    if post_pause:
        press_enter_to_continue()
