# This is the base directory where all the Easy ANSI projects are cloned to.
BASE_DIR = "/home/joey/Development/GitLab/easy-ansi_group"

# This is the root directory of the development tools code and scripts.
DEV_TOOLS_DIR = f"{BASE_DIR}/development-tools-for-easy-ansi"

# This is the directory of the linux scripts
LINUX_SCRIPTS_DIR = f"{DEV_TOOLS_DIR}/linux_scripts"

# This is the directory of the linux scripts for checking code quality and running unit tests
CODE_QUALITY_LINUX_SCRIPTS_DIR = f"{LINUX_SCRIPTS_DIR}/code_quality"

# This is the directory of the linux scripts for building and deploying.
BUILD_AND_DEPLOY_LINUX_SCRIPTS_DIR = f"{LINUX_SCRIPTS_DIR}/build_deploy"

# This is the name of the python virtual environment used to develop the Easy ANSI projects.
# Set this to an empty string to use the current python environment.
PYTHON_ENV = "easyansi_env"

# This is the base directory where PyPI tokens are saved
PYPI_CREDENTIALS_DIR = "/data/credentials/pypi"

# Easy ANSI Projects Configuration
PROJECTS = {
    "Easy ANSI Core": {"base_dir": f"{BASE_DIR}/easy-ansi",
                       "source_dir": "easyansi",
                       "tests_dir": "tests",
                       "demos_dir": "demos",
                       "egg_name": "easy_ansi",
                       "test_pypi_token_file": "test_pypi_easyansi_token.txt",
                       "prod_pypi_token_file": "PRODUCTION_pypi_easyansi_token.txt"},

    "Easy ANSI Color Packs": {"base_dir": "easy-ansi-color-packs"},

    "Easy ANSI Widgets": {"base_dir": "easy-ansi-widgets"}}
