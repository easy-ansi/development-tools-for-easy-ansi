CLEAR_SCREEN = '\033' + "[2J" + '\033' + "[1;1H"


# Tiny Colors: https://gitlab.com/joeysbytes/python-snippets/-/tree/master/Tiny_Colors
def color16(fg: int = None,  bg: int = None) -> str:
    code = ""
    if (fg is not None) and (isinstance(fg, int)):
        fg_code = 30 + fg if 0 <= fg <= 7 else 82 + fg if 8 <= fg <= 15 else 39; code += f"\033[{fg_code}m"
    if (bg is not None) and (isinstance(bg, int)):
        bg_code = 40 + bg if 0 <= bg <= 7 else 92 + bg if 8 <= bg <= 15 else 49; code += f"\033[{bg_code}m"
    return code


def heading(text: str, clear_screen: bool = True) -> str:
    width = 75
    line_char = '='
    default_color = color16(-1, -1)
    line_color = color16(5, 0)
    heading_color = color16(6, 0)
    if len(text) < width:
        text = text + " " * (width - len(text))
    hdg = ""
    if clear_screen:
        hdg += CLEAR_SCREEN
    hdg += line_color + line_char * width + default_color + '\n'
    hdg += heading_color + text + default_color + '\n'
    hdg += line_color + line_char * width + default_color + '\n'
    return hdg


def press_enter_to_continue():
    msg = color16(0, 10) + "Press ENTER to continue..." + color16(-1, -1) + " "
    print(color16(-1, -1))
    input(msg)


# https://gitlab.com/joeysbytes/python-snippets/-/tree/master/Tiny_Yes_No
def yes_no(prompt: str) -> bool:
    while True:
        try:
            answer = input(f"{prompt} (y/n): ")[0:1].lower()
            if (answer != "y") and (answer != "n"):
                raise ValueError()
            return True if answer == "y" else False
        except Exception:
            print("\nInvalid Answer: Please type 'y' or 'n'\n")
