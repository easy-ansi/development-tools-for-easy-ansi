from typing import List
from tools.text import heading
from tools.config import PROJECTS
from menus.menu import menu
from tools import script_runners


def tools_menu(project: str):
    hdg = f"Development Tools: {project}"
    menu_items = build_menu_items(project)
    last_idx = len(menu_items) - 1
    menu_idx = -1
    while menu_idx != last_idx:
        print(heading(hdg))
        menu_idx = menu(menu_items)
        if menu_idx != last_idx:
            process_menu_choice(menu_items[menu_idx], project)


def build_menu_items(project: str) -> List:
    menu_items = []
    project_cfg = PROJECTS[project]
    if "tests_dir" in project_cfg:
        menu_items.append("Run PyTest")
    if "source_dir" in project_cfg:
        menu_items.append("Run MyPy")
        menu_items.append("Run Flake8")
    if "demos_dir" in project_cfg:
        menu_items.append("Run Demos")
    if "source_dir" in project_cfg:
        menu_items.append("Install Locally")
        menu_items.append("Build for Deployment")
        menu_items.append("Clean Build Files")
        menu_items.append("Deploy to Test PyPI")
        menu_items.append("Deploy to PRODUCTION PyPI")
    menu_items.append("Back to Main Menu")
    return menu_items


def process_menu_choice(menu_choice: str, project: str):
    if menu_choice == "Run PyTest":
        script_runners.run_pytest(project)
    elif menu_choice == "Run MyPy":
        script_runners.run_mypy(project)
    elif menu_choice == "Run Flake8":
        script_runners.run_flake8(project)
    elif menu_choice == "Run Demos":
        pass
    elif menu_choice == "Install Locally":
        script_runners.install_locally(project)
    elif menu_choice == "Build for Deployment":
        script_runners.full_build(project)
    elif menu_choice == "Clean Build Files":
        script_runners.clean_build_files(project)
    elif menu_choice == "Deploy to Test PyPI":
        script_runners.publish_pypi(project, True)
    elif menu_choice == "Deploy to PRODUCTION PyPI":
        script_runners.publish_pypi(project, False)
