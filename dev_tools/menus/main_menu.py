from typing import List
from tools.text import heading
from menus.menu import menu
from menus.tools_menu import tools_menu
from tools.config import PROJECTS


def main_menu():
    menu_items = build_menu_items()
    last_idx = len(menu_items) - 1
    menu_idx = -1
    while menu_idx != last_idx:
        print(heading("Main Menu: Easy ANSI Development Tools"))
        menu_idx = menu(menu_items)
        if menu_idx != last_idx:
            process_menu_choice(menu_items[menu_idx])


def build_menu_items() -> List:
    menu_items = list(PROJECTS.keys())
    menu_items.append("Quit")
    return menu_items


def process_menu_choice(menu_choice):
    tools_menu(menu_choice)
