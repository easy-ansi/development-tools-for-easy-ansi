# Starting at a base directory, delete all directories with a given name
function delete_all_dirs() {
    local base_dir="${1}"
    local dir_name="${2}"

    cd "${base_dir}" || { echo "Directory Not Found: ${base_dir}"; exit 1; }

    while IFS= read -r d
    do
        echo "Deleting Directory: ${d}"
        rm -rf "${d}"
    done < <(find "${base_dir}" -type d -name "${dir_name}")
}


# Starting at a base directory, delete all files with a given name
function delete_all_files() {
    local base_dir="${1}"
    local file_name="${2}"

    cd "${base_dir}" || { echo "Directory Not Found: ${base_dir}"; exit 1; }

    while IFS= read -r f
    do
        echo "Deleting File: ${f}"
        rm "${f}"
    done < <(find "${base_dir}" -type f -name "${file_name}")
}


# Given a specific directory, delete it
function delete_dir() {
    local dir_name="${1}"
    if [ -d "${dir_name}" ]
    then
        echo "Deleting Dir: ${dir_name}"
        rm -rf "${dir_name}"
  fi
}
