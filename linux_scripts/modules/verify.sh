# Verify that a directory exists
function directory_must_exist() {
    local dir_name="${1}"
    if [ ! -d "${dir_name}" ]
    then
        echo "ERROR: Directory not found: ${dir_name}"
        exit 1
    fi
}


# Verify that a file exists
function file_must_exist() {
    local file_name="${1}"
    if [ ! -f "${file_name}" ]
    then
        echo "ERROR: File not found: ${file_name}"
        exit 1
    fi
}
