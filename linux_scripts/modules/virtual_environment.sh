# Function to activate a virtualized python environment
function activate_environment() {
    # Empty env_name means to not change anything
    local env_name="${1}"
    local status

    if [ ! "${env_name}" == "" ]
    then
        # Check if we are using conda
        set +e
        which conda>/dev/null 2>&1
        status=$?
        set -e
        if [ "${status}" -eq 0 ]
        then
            activate_conda_environment "${env_name}"
        else
            echo "NOTE: No Python Virtualization Method Found, not changing to: ${env_name}"
        fi
    else
        echo "No Python Virtual Environment is being used."
    fi
}


# Make sure we are in the given conda environment
function activate_conda_environment() {
    local conda_env="${1}"
    local current_env
    # shellcheck disable=SC2063
    current_env="$(conda env list|grep '*'|cut -f1 -d\ )"
    echo "Current Conda Environment: ${current_env}"
    if [ "${current_env}" != "${conda_env}" ]
    then
        source activate "${conda_env}"
        # shellcheck disable=SC2063
        current_env="$(conda env list|grep '*'|cut -f1 -d\ )"
        echo "New Conda Environment:     ${current_env}"
    fi
    echo ""
}
