#!/usr/bin/env bash
set -e


###########################################################################
# Passed-In Arguments
###########################################################################
PYTHON_ENV="${1}"
SOURCE_DIR="${2}"
FLAKE8_INI="${3}"
FLAKE8_STATUS=0


###########################################################################
# Source In Modules
###########################################################################
source ../modules/virtual_environment.sh
source ../modules/verify.sh


###########################################################################
# Processing Functions
###########################################################################
function main() {
    show_variables
    verify_variables
    activate_environment "${PYTHON_ENV}"
    run_flake8
    exit "${FLAKE8_STATUS}"
}


function show_variables() {
    echo "Running Flake8:"
    echo "  Source Dir: ${SOURCE_DIR}"
    echo "  INI File:   ${FLAKE8_INI}"
    echo "Python Env:   ${PYTHON_ENV}"
}


function verify_variables() {
    directory_must_exist "${SOURCE_DIR}"
    file_must_exist "${FLAKE8_INI}"
}


function run_flake8() {
    echo "Running Flake8..."
    cd "${SOURCE_DIR}"
    set +e
    python3 -m flake8 --config="${FLAKE8_INI}" "${SOURCE_DIR}"
    FLAKE8_STATUS=$?
    set -e
    echo ""
    echo "Flake8 Exit Code: ${FLAKE8_STATUS}"
    echo ""
}


main
