#!/usr/bin/env bash
set -e


###########################################################################
# Passed-In Arguments
###########################################################################
PYTHON_ENV="${1}"
SOURCE_DIR="${2}"
MYPY_INI="${3}"
MYPY_STATUS=0


###########################################################################
# Source In Modules
###########################################################################
source ../modules/virtual_environment.sh
source ../modules/verify.sh


###########################################################################
# Processing Functions
###########################################################################
function main() {
    show_variables
    verify_variables
    activate_environment "${PYTHON_ENV}"
    run_mypy
    exit "${MYPY_STATUS}"
}


function show_variables() {
    echo "Running MyPy:"
    echo "  Source Dir: ${SOURCE_DIR}"
    echo "  INI File:   ${MYPY_INI}"
    echo "Python Env:   ${PYTHON_ENV}"
}


function verify_variables() {
    directory_must_exist "${SOURCE_DIR}"
    file_must_exist "${MYPY_INI}"
}


function run_mypy() {
    echo "Running MyPy..."
    cd "${SOURCE_DIR}"
    set +e
    python3 -m mypy --strict --config-file "${MYPY_INI}" "${SOURCE_DIR}"
    MYPY_STATUS=$?
    set -e
    echo ""
    echo "MyPy Exit Code: ${MYPY_STATUS}"
    echo ""
}


main
