#!/usr/bin/env bash
set -e

# Notes:
#   -s: add to command to see output of print statements
#   Required: https://pytest-cov.readthedocs.io/en/latest/ , pytest-cov


###########################################################################
# Passed-In Arguments
###########################################################################
PYTHON_ENV="${1}"
PROJECT_DIR="${2}"
SOURCE_DIR="${3}"
TESTS_DIR="${4}"
PYTEST_STATUS=0


###########################################################################
# Source In Modules
###########################################################################
source ../modules/virtual_environment.sh
source ../modules/cleanup.sh
source ../modules/verify.sh


###########################################################################
# Processing Functions
###########################################################################
function main() {
    show_variables
    activate_environment "${PYTHON_ENV}"
    verify_variables
    run_pytest
    clean_up_cache
    exit "${PYTEST_STATUS}"
}


function show_variables() {
    echo "Running PyTest:"
    echo "  Project Dir: ${PROJECT_DIR}"
    echo "  Source Dir:  ${SOURCE_DIR}"
    echo "  Tests Dir:   ${TESTS_DIR}"
    echo "Python Env:    ${PYTHON_ENV}"
}


function verify_variables() {
    directory_must_exist "${PROJECT_DIR}"
    directory_must_exist "${SOURCE_DIR}"
    directory_must_exist "${TESTS_DIR}"
}

function run_pytest() {
    echo "Running PyTest..."
    cd "${SOURCE_DIR}"
    set +e
    python3 -m pytest --cache-clear --cov=easyansi --cov-report=term-missing "${TESTS_DIR}/"
    PYTEST_STATUS=$?
    set -e
    echo ""
    echo "PyTest Exit Code: ${PYTEST_STATUS}"
    echo ""
}


function clean_up_cache() {
    echo "Cleaning up caches..."
    delete_all_dirs "${PROJECT_DIR}" ".pytest_cache"
    delete_all_files "${PROJECT_DIR}" ".coverage"
}


main
