#!/usr/bin/env bash
set -e


###########################################################################
# Passed-In Arguments
###########################################################################
PYTHON_ENV="${1}"
PROJECT_DIR="${2}"


###########################################################################
# Source In Modules
###########################################################################
source ../modules/virtual_environment.sh
source ../modules/verify.sh


###########################################################################
# Processing Functions
###########################################################################
function main() {
    show_variables
    verify_variables
    activate_environment "${PYTHON_ENV}"
    install_locally
}


function show_variables() {
    echo "Install Locally:"
    echo "  Project Dir: ${PROJECT_DIR}"
    echo "Python Env:    ${PYTHON_ENV}"
}


function verify_variables() {
    directory_must_exist "${PROJECT_DIR}"
}


function install_locally() {
    echo "Running installer..."
    cd "${PROJECT_DIR}"
    python -m pip install --editable .
}


main
