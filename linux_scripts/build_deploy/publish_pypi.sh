#!/usr/bin/env bash
set -e


###########################################################################
# Passed-In Arguments
###########################################################################
PYTHON_ENV="${1}"
PROJECT_DIR="${2}"
# test or PRODUCTION
PYPI_ENV="${3}"
PYPI_TOKEN_FILE="${4}"


###########################################################################
# Source In Modules
###########################################################################
source ../modules/virtual_environment.sh
source ../modules/verify.sh


###########################################################################
# Processing Functions
###########################################################################
function main() {
    show_variables
    verify_variables
    activate_environment "${PYTHON_ENV}"
    # publish_to_pypi
}


function show_variables() {
    echo "Publish to PyPI:"
    echo "  Project Dir:     ${PROJECT_DIR}"
    echo "  PyPI Env:        ${PYPI_ENV}"
    echo "  PyPI Token File: ${PYPI_TOKEN_FILE}"
    echo "Python Env:        ${PYTHON_ENV}"
}


function verify_variables() {
    directory_must_exist "${PROJECT_DIR}"
    file_must_exist "${PYPI_TOKEN_FILE}"
    if [ ! "${PYPI_ENV}" == "test" ] && [ ! "${PYPI_ENV}" == "PRODUCTION" ]
    then
        echo "ERROR: Invalid PyPI Environment: ${PYPI_ENV}"
        echo "  Valid values: test, PRODUCTION"
        exit 1
    fi
}


function publish_to_pypi() {
    echo "Publishing to PyPI: ${PYPI_ENV}..."
    cd "${PROJECT_DIR}"
    local pypi_repository
    local publish_token
    if [ "${PYPI_ENV}" == "test" ]
    then
        pypi_repository="testpypi"
    elif [ "${PYPI_ENV}" == "PRODUCTION" ]
    then
        pypi_repository="pypi"
    else
        pypi_repository="UNKNOWN"
    fi
    publish_token=$(cat "${PYPI_TOKEN_FILE}")
    twine upload \
        --repository "${pypi_repository}" \
        --disable-progress-bar \
        --username "__token__" \
        --password "${publish_token}" \
        dist/*
    publish_token=""
}


main
