#!/usr/bin/env bash
set -e

# pip install build, twine


###########################################################################
# Passed-In Arguments
###########################################################################
PYTHON_ENV="${1}"
PROJECT_DIR="${2}"
EGG_NAME="${3}"


###########################################################################
# Source In Modules
###########################################################################
source ../modules/virtual_environment.sh
source ../modules/verify.sh
source ../modules/cleanup.sh


###########################################################################
# Processing Functions
###########################################################################
function main() {
    show_variables
    verify_variables
    activate_environment "${PYTHON_ENV}"
    clean_build_files
}


function show_variables() {
    echo "Clean Build Files:"
    echo "  Project Dir: ${PROJECT_DIR}"
    echo "  Egg Name:    ${EGG_NAME}"
    echo "Python Env:    ${PYTHON_ENV}"
}


function verify_variables() {
    directory_must_exist "${PROJECT_DIR}"
}


function clean_build_files() {
    echo "Performing file cleanup..."
    cd "${PROJECT_DIR}"

    delete_dir "build"
    delete_dir "dist"
    delete_dir "${EGG_NAME}.egg-info"
}


main
