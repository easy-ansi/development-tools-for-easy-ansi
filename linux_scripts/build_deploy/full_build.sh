#!/usr/bin/env bash
set -e

# pip install build, twine


###########################################################################
# Passed-In Arguments
###########################################################################
PYTHON_ENV="${1}"
PROJECT_DIR="${2}"


###########################################################################
# Source In Modules
###########################################################################
source ../modules/virtual_environment.sh
source ../modules/verify.sh


###########################################################################
# Processing Functions
###########################################################################
function main() {
    show_variables
    verify_variables
    activate_environment "${PYTHON_ENV}"
    full_build
}


function show_variables() {
    echo "Perform Full Build:"
    echo "  Project Dir: ${PROJECT_DIR}"
    echo "Python Env:    ${PYTHON_ENV}"
}


function verify_variables() {
    directory_must_exist "${PROJECT_DIR}"
}


function full_build() {
    echo "Performing full build..."
    cd "${PROJECT_DIR}"

    # Source distribution
    echo "--- Build Source Distribution"
    python -m build --sdist

    # Wheel distribution
    echo "--- Build Wheel Distribution"
    python -m build --wheel

    # Have twine check for common PyPI issues
    echo "--- Twine check for issues"
    twine check dist/*
}


main
